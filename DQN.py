import gym
import collections
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt

learning_rate = 0.0005
gamma = 0.98
buffer_limit = 50000
batch_size = 32
x = []
y = []


class QNet(nn.Module):
    def __init__(self):
        super(QNet, self).__init__()
        self.fc1 = nn.Linear(4, 128)
        self.fc2 = nn.Linear(128, 128)
        self.fc3 = nn.Linear(128, 2)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def sample_action(self, obs, epsilon):
        out = self.forward(obs)
        if random.random() < epsilon:
            return random.randint(0, 1)
        else:
            return out.argmax().item()


class ReplayBuffer:
    def __init__(self):
        self.buffer = collections.deque(maxlen=buffer_limit)

    def put(self, transition):
        self.buffer.append(transition)

    def sample_batch_step(self, n):
        mini_batch = random.sample(self.buffer, n)
        s_lst, a_lst, r_lst, s_prime_lst, done_mask_lst = [], [], [], [], []
        for transition in mini_batch:
            s, a, r, s_prime, done_mask = transition
            s_lst.append(s)
            a_lst.append([a])
            r_lst.append([r])
            s_prime_lst.append(s_prime)
            done_mask_lst.append([done_mask])

        return torch.tensor(s_lst, dtype=torch.float), torch.tensor(a_lst), \
               torch.tensor(r_lst, dtype=torch.float), torch.tensor(s_prime_lst, dtype=torch.float), \
               torch.tensor(done_mask_lst, dtype=torch.float)

    def size(self):
        return len(self.buffer)


# def train(q_net, replay_buffer, optimizer):
def train(q_net, q_net_target, replay_buffer, optimizer):
    for i in range(10):
        s, a, r, s_prime, done_mask = replay_buffer.sample_batch_step(batch_size)
        q_prediction = q_net(s)
        a_q = q_prediction.gather(1, a)
        # max_q_prime = q_net(s_prime).max(1)[0].unsqueeze(1)
        max_q_prime = q_net_target(s_prime).max(1)[0].unsqueeze(1)
        target = r + gamma * max_q_prime * done_mask
        loss = F.smooth_l1_loss(a_q, target)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


def main():
    env = gym.make('CartPole-v1')
    q_net = QNet()
    q_net_target = QNet()
    q_net_target.load_state_dict(q_net.state_dict())
    replay_buffer = ReplayBuffer()

    print_interval = 20
    score = 0.0
    optimizer = optim.Adam(q_net.parameters(), lr=learning_rate)

    for n_epi in range(10000):
        epsilon = max(0.01, 0.08 - 0.01 * (n_epi / 200))
        s = env.reset()
        done = False

        while not done:
            a = q_net.sample_action(torch.from_numpy(s).float(), epsilon)
            s_prime, r, done, info = env.step(a)
            done_mask = 0.0 if done else 1.0
            replay_buffer.put((s, a, r / 100.0, s_prime, done_mask))
            s = s_prime

            score += r
            if done:
                break

        if replay_buffer.size() > 2000:
            # train(q_net, replay_buffer, optimizer)
            train(q_net, q_net_target, replay_buffer, optimizer)

        if n_epi % print_interval == 0 and n_epi != 0:
            q_net_target.load_state_dict(q_net.state_dict())
            print("n_episode :{}, score : {:.1f}, n_buffer : {}, eps : {:.1f}%".format(
                n_epi, score / print_interval, replay_buffer.size(), epsilon * 100))
            x.append(n_epi)
            y.append(score)
            score = 0.0

    env.close()


if __name__ == '__main__':
    main()
    plt.plot(x, y)
    plt.show()
